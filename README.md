# README #

This project is a News website written in ReactJS


### How do I get set up? ###

First of all clone this repository:
```
git clone
```

Install the dependencies:
```
npm install
```

run in developer mode:
```
npm start
```


### Deploying the application

The application is hosted on Firebase hosting, and for deploy, you need the CLI tool.
 ```
 npm install -g firebase-tool
 ```

In order to publish the app you need to follow these steps:

1) Generate the optimized files:
```
npm build
```

2) Send the files upstream
```
firebase deploy
```
