import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { mockInstall } from "./mock";
import NewsWebsite from "./containers/news-website";

mockInstall();

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <NewsWebsite />
      </BrowserRouter>
    );
  }
}

export default App;
