export const mockInstall = () => {
  var axios = require("axios");
  var MockAdapter = require("axios-mock-adapter");

  // This sets the mock adapter on the default instance
  // var mock = new MockAdapter(axios, { delayResponse: 2000 });
  var mock = new MockAdapter(axios);

  const news_01 = require("./assets/news_01.jpg");
  const news_01_2x = require("./assets/news_01@2x.png");
  const news_02 = require("./assets/news_02.jpg");
  const news_02_2x = require("./assets/news_02@2x.jpg");
  const news_03 = require("./assets/news_03.jpg");
  const news_03_2x = require("./assets/news_03@2x.jpg");

  const author_photo = require("./assets/author_photo.jpg");
  mock.onGet("/news").reply(200, [
    {
      id: 1,
      title: "Obama Offers Hopeful Vision While Noting Nation's Fears",
      description:
        "In his last State of the Union addres, President Obama sought to paint a hopeful portrait. But he acknowledged that many Americans felt shut out of a political and economic system they view as rigged.",
      author: { name: "Creed Bratton", photo: author_photo },
      image: news_01,
      image_hidpi: news_01_2x,
      tag: "politics"
    },
    {
      id: 2,
      title:
        "Didi Kuaidi, The Company Beating Uber In China, Opens Its API To Third Party Apps",
      description:
        "One day after Uber updated its API to add ‘content experiences’ for passengers, the U.S. company’s biggest rival — Didi Kuaidi in China — has opened its own platform up by releasing an SDK for developers and third-parties.",
      author: { name: "Creed Bratton", photo: author_photo },
      image: news_02,
      image_hidpi: news_02_2x,
      tag: "tech"
    },
    {
      id: 3,
      title: "NASA Formalizes Efforts To Protect Earth From Asteroids",
      description:
        "Last week, NASA announced a new program called the Planetary Defense Coordination Office (PDCO) which will coordinate NASA’s efforts for detecting and tracking near-Earth objects (NEOs). If a large object comes hurtling toward our planet…",
      author: {
        name: "Alexandre Henrique Shailesh Zeta-Jones",
        photo: author_photo
      },
      image: news_03,
      image_hidpi: news_03_2x,
      tag: "science"
    },
    {
      id: 4,
      title:
        "For Some Atlanta Hawks, a Revved-Up Game of Uno Is Diversion No. 1",
      description:
        "The favored in-flight pastime of a group of players including Al Horford, Kent Bazemore and Dennis Schroder is a schoolchildren’s card game with some added twists.",
      author: { name: "Creed Bratton", photo: author_photo },
      image: news_02,
      image_hidpi: news_02_2x,
      tag: "sports"
    },
    {
      id: 5,
      title: "Picking a Windows 10 Security Package",
      description:
        "Oscar the Grouch has a recycling bin and Big Bird has moved to a tree as the children’s classic debuts on HBO, aiming at a generation that doesn’t distinguish between TV and mobile screens.",
      author: { name: "Creed Bratton", photo: author_photo },
      image: news_02,
      image_hidpi: news_02_2x,
      tag: "tech"
    },
    {
      id: 6,
      title: "As U.S. Modernizes Nuclear Weapons, ‘Smaller’ Leaves Some Uneasy",
      description:
        "The Energy Department and the Pentagon have been readying a weapon with a build-it-smaller approach, setting off a philosophical clash in the world of nuclear arms.",
      author: { name: "Creed Bratton", photo: author_photo },
      image: news_02,
      image_hidpi: news_02_2x,
      tag: "science"
    }
  ]);
};
