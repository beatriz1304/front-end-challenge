import React, { Component } from "react";
import { Route } from "react-router-dom";
import classes from "./news-website.css";
import NewsGrid from "./news-grid";
import Toolbar from "../components/navigation/toolbar/toolbar";
import SideDrawer from "../components/navigation/side-drawer/side-drawer";
import Login from "../components/login/login";
import Interests from "../components/interests/interests";

class NewsWebsite extends Component {
  state = {
    showSideDrawer: false
  };

  sideDrawerClosedHandler = () => {
    this.setState({ showSideDrawer: false });
  };

  sideDrawerToggleHandler = () => {
    this.setState(prevState => {
      return { showSideDrawer: !prevState.showSideDrawer };
    });
  };

  render() {
    return (
      <div>
        <Toolbar drawerToggleClicked={this.sideDrawerToggleHandler} />
        <SideDrawer
          open={this.state.showSideDrawer}
          closed={this.sideDrawerClosedHandler}
        />
        <main className={classes.NewsWebsite}>
          <Route path="/" exact component={NewsGrid} />
          <Route path="/login" exact component={Login} />
          <Route path="/:type" exact component={NewsGrid} />
          <Route path="/interests/:userName" exact component={Interests} />
        </main>
      </div>
    );
  }
}

export default NewsWebsite;
