import React, { Component } from "react";
import axios from "axios";

import classes from "./news-grid.css";
import NewsItem from "../components/news/news-item";

const TAG_COLOR = {
  politics: "#FF001F",
  business: "#fff",
  tech: "#4990E2",
  science: "#7CBC37",
  sports: "#F6A623"
};

class NewsGrid extends Component {
  state = {
    news: [],
    newsType: "",
    currentTag: ""
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      const nextTag = nextProps.match.params.type;
      this.retrieveNewsData(nextTag);
    }
  }

  retrieveNewsData(
    tagList = ["politics", "business", "tech", "science", "sports"]
  ) {
    axios
      .get("/news")
      .then(response => {
        const newsList = response.data;
        const updatedNews = newsList
          .filter(newsItem => tagList.includes(newsItem.tag))
          .map(newsItem => {
            return { ...newsItem };
          });
        this.setState({ news: updatedNews });
      })
      .catch(error => {
        console.log("Não foi possível carregar os dados os dados", error);
      });
  }

  componentDidMount() {
    this.setState({ newsType: this.props.match.params.type });
    let type = this.props.match.params.type;
    type ? this.retrieveNewsData(type) : this.retrieveNewsData();
  }

  render() {
    let newsList = <p style={{ textAlign: "center" }}>Something went wrong!</p>;
    if (!this.state.error) {
      newsList = this.state.news.map(news => {
        const image =
          window.devicePixelRatio > 1 ? news.image_hidpi : news.image;
        return (
          <NewsItem
            key={news.id}
            category={news.tag}
            title={news.title}
            description={news.description}
            image={image}
            categoryColor={TAG_COLOR[news.tag]}
            author={news.author}
          />
        );
      });
    }
    return (
      <section className={classes.NewsGrid}>
        {newsList.slice(0, 3)}
        <div
          length={newsList.length > 3 ? "true" : "false"}
          className={classes.HorizontalLine}
        />
        {newsList.slice(3)}
      </section>
    );
  }
}

export default NewsGrid;
