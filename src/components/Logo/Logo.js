import React from "react";
import { Link } from "react-router-dom";

import newsLogo from "../../assets/logo.png";
import classes from "./logo.css";

const logo = () => (
  <div className={classes.Logo}>
    <Link to={{ pathname: "/" }}>
      <img src={newsLogo} alt="News" />
    </Link>
  </div>
);

export default logo;
