import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import classes from "./login.css";
import Button from "../ui/button/button";

class Login extends Component {
  state = {
    userName: "",
    password: "",
    submitted: false
  };

  handleClick = () => {
    if (this.state.userName !== "" && this.state.password !== "") {
      this.setState({ submitted: true });
    }
  };

  redirectToInterests = () => {
    const { submitted } = this.state;
    let redirect = null;
    if (submitted) {
      redirect = <Redirect to={"/interests/" + this.state.userName} />;
    }
    return redirect;
  };

  render() {
    return (
      <div className={classes.Container}>
        {this.redirectToInterests()}
        <div className={classes.Title}>User Area</div>
        <form className={classes.Form}>
          <label className={classes.Label}>Email</label>
          <input
            className={classes.Input}
            type="email"
            value={this.state.userName}
            onChange={event => this.setState({ userName: event.target.value })}
          />
          <label className={classes.Label}>Password</label>
          <input
            className={classes.Input}
            value={this.state.password}
            onChange={event => this.setState({ password: event.target.value })}
          />
          <Button label="Login" clicked={this.handleClick} />
        </form>
      </div>
    );
  }
}

export default Login;
