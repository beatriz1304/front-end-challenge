import React from "react";

import classes from "./news-item.css";

const NewsItem = ({
  categoryColor,
  category,
  image,
  title,
  author,
  description
}) => (
  <article className={classes.NewsItem}>
    <span className={classes.Category} style={{ color: categoryColor }}>
      {category}
    </span>
    <div className={classes.ImageWrapper}>
      <img className={classes.Image} src={image} alt="img" />
      <div className={classes.ButtonOverlay}>
        <button className={classes.Button}>Read More</button>
      </div>
    </div>
    <h2 className={classes.Title}>{title}</h2>
    <div className={classes.Author}>
      <img src={author.photo} alt="img" />
      {"by " + author.name}
    </div>
    <p className={classes.Description}>{description}</p>
  </article>
);

export default NewsItem;
