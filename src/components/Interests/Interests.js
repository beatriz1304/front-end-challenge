import React, { Component } from "react";
import { Link } from "react-router-dom";

import classes from "./interests.css";
import InterestButton from "../ui/interest/interest-button";
import Button from "../ui/button/button";

class Interests extends Component {
  state = {
    userName: ""
  };
  componentDidMount() {
    this.setState({ userName: this.props.match.params.userName });
  }

  render() {
    return (
      <div className={classes.Container}>
        <div className={classes.Title}>Welcome, {this.state.userName}</div>
        <div className={classes.Interests}>
          <label className={classes.InterestsLabel}>My Interests</label>
          <div className={classes.Items}>
            <InterestButton label="Politics" />
            <InterestButton label="Business" />
            <InterestButton label="Tech" />
            <InterestButton label="Science" />
            <InterestButton label="Sports" />
          </div>
        </div>
        <Link to={{ pathname: "/" }}>
          <Button label="Save" />
        </Link>
        <Link to={{ pathname: "/" }}>
          <button className={classes.BackToHomeButton} btntype="Send">
            Back to home
          </button>
        </Link>
      </div>
    );
  }
}

export default Interests;
