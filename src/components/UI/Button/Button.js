import React from "react";

import classes from "./button.css";

const button = ({ clicked, label }) => (
  <button className={classes.Button} type="button" onClick={clicked}>
    {label}
  </button>
);

export default button;
