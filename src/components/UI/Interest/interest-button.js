import React, { Component } from "react";
import classes from "./interest-button.css";

class Interest extends Component {
  state = {
    active: false
  };

  toggle = () => {
    this.setState({ active: !this.state.active });
  };

  render() {
    let interestColor = null;
    switch (this.props.label) {
      case "Politics":
        interestColor = (
          <button
            className={
              classes.Button +
              " " +
              (this.state.active ? classes.RedActive : classes.Red)
            }
            btntype="Send"
            onClick={this.toggle}
          >
            {this.props.label}
          </button>
        );
        break;
      case "Business":
        interestColor = (
          <button
            className={
              classes.Button +
              " " +
              (this.state.active ? classes.PurpleActive : classes.Purple)
            }
            btntype="Send"
            onClick={this.toggle}
          >
            {this.props.label}
          </button>
        );
        break;
      case "Tech":
        interestColor = (
          <button
            className={
              classes.Button +
              " " +
              (this.state.active ? classes.BlueActive : classes.Blue)
            }
            btntype="Send"
            onClick={this.toggle}
          >
            {this.props.label}
          </button>
        );
        break;
      case "Science":
        interestColor = (
          <button
            className={
              classes.Button +
              " " +
              (this.state.active ? classes.GreenActive : classes.Green)
            }
            btntype="Send"
            onClick={this.toggle}
          >
            {this.props.label}
          </button>
        );
        break;
      case "Sports":
        interestColor = (
          <button
            className={
              classes.Button +
              " " +
              (this.state.active ? classes.OrangeActive : classes.Orange)
            }
            btntype="Send"
            onClick={this.toggle}
          >
            {this.props.label}
          </button>
        );
        break;
      default:
        interestColor = null;
    }
    return interestColor;
  }
}

export default Interest;
