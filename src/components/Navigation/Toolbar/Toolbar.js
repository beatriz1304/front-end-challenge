import React from "react";

import classes from "./toolbar.css";
import Logo from "../../logo/logo";
import NavigationItems from "../navigation-items/navigation-items";
import DrawerToggle from "../side-drawer/drawer-toggle/drawer-toggle";

const toolbar = ({ drawerToggleClicked }) => (
  <header className={classes.Toolbar}>
    <div className={classes.ToolbarContainer}>
      <div className={classes.MobileOnly}>
        <DrawerToggle clicked={drawerToggleClicked} />
      </div>
      <Logo />
      <div className={classes.MobileOnly} />
      <nav className={classes.DesktopOnly}>
        <NavigationItems />
      </nav>
    </div>
  </header>
);

export default toolbar;
