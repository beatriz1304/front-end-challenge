import React from "react";

import menu from "../../../../assets/menu.png";
import classes from "./drawer-toggle.css";

const drawerToggle = ({ clicked }) => (
  <div onClick={clicked} className={classes.DrawerToggle}>
    <img src={menu} alt="DrawerToggle" />
  </div>
);

export default drawerToggle;
