import React from "react";

import NavigationItems from "../navigation-items/navigation-items";
import classes from "./side-drawer.css";
import Backdrop from "../../ui/backdrop/backdrop";

const sideDrawer = ({ open, closed }) => {
  let attachedClasses = [classes.SideDrawer, classes.Close];
  if (open) {
    attachedClasses = [classes.SideDrawer, classes.Open];
  }

  return (
    <div>
      <Backdrop show={open} clicked={closed} />
      <div className={attachedClasses.join(" ")}>
        <NavigationItems />
      </div>
    </div>
  );
};

export default sideDrawer;
