import React from "react";

import classes from "./navigation-items.css";
import NavigationItem from "./navigation-item/navigation-item";

const navigationItems = () => (
  <ul className={classes.NavigationItems}>
    <NavigationItem link="/politics">Politics</NavigationItem>
    <NavigationItem link="/business">Business</NavigationItem>
    <NavigationItem link="/tech">Tech</NavigationItem>
    <NavigationItem link="/science">Science</NavigationItem>
    <NavigationItem link="/sports">Sports</NavigationItem>
    <NavigationItem link="/login">Login</NavigationItem>
  </ul>
);

export default navigationItems;
