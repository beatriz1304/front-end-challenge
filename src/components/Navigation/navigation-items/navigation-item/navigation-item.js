import React from "react";
import { Link } from "react-router-dom";

import classes from "./navigation-item.css";

const navigationItem = ({ link, active, children }) => (
  <li className={classes.NavigationItem}>
    <Link to={{ pathname: link }} className={active ? classes.active : null}>
      {children}
    </Link>
  </li>
);

export default navigationItem;
